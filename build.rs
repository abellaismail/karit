use std::path::PathBuf;

fn main() {
    let dir: PathBuf;

    cargo_emit::rerun_if_changed!("build.rs", "tree_sitter-c/src/parser.c", "./Cargo.lock",);

    dir = ["tree-sitter-c", "src"].iter().collect();
    cc::Build::new()
        .include(&dir)
        .file(dir.join("parser.c"))
        .compile("tree-sitter-c");
}
