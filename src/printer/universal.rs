use crate::printer::{declarator, utils};
use crate::source::Source;

use regex::Regex;
use tree_sitter::Node;

use super::for_statment;
use super::utils::indent;

fn pretty_children(node: &Node, src: &mut Source) -> String {
    let kind = node.kind();
    node.children(&mut node.walk())
        .filter(|item| !(item.kind() == "{" && kind == "compound_statement"))
        .map(|item| pretty(&item, src))
        .collect::<Vec<String>>()
        .join("")
}

pub fn pretty(node: &Node, src: &mut Source) -> String {
    let token = utils::get_text(&node, src);
    let kind = node.kind();
    let re = Regex::new(r"\s+").unwrap();

    if kind.trim().is_empty() {
        return String::new();
    }

    match kind {
        "declaration" => declarator::pretty_take_init(node, src),
        "for_statement" => for_statment::for_convert(node, src),
        "primitive_type" => {
            let mut str: String = re.replace_all(token, " ").into();
            if node.parent().unwrap().kind() == "parameter_declaration" {
                if node.next_named_sibling().is_some() {
                    str.push(' ');
                }
            } else {
                str.push('\t');
            }
            str
        }
        "compound_statement" => {
            let mut bracket: String = String::new();
            let need_brackets = node.parent().unwrap().kind() == "function_definition"
                || node.named_child_count() != 1;
            if need_brackets {
                bracket = format!("\n{}{{\n", indent(node, src, &|_, _src| { String::new() }));
            }
            else if node.parent().unwrap().kind() != "for_statement"
            {
                bracket = String::from("\n");
            }
            
            src.level += 1;
            let ret = format!(
                "{}{}{}",
                bracket,
                declarator::pretty_declarations(node, src),
                pretty_children(node, src)
            );
            src.level -= 1;
            if need_brackets {
                bracket = indent(node, src, &|_, _src| String::from("}\n"));
            }
            format!("{}{}\n", ret, bracket)
        }
        "if" | "while" => indent(node, src, &|_, _src| format!("{} ", node.kind())),
        "continue_statement" | "break_statement" | "return_statement" => {
            indent(node, src, &|_, _src| {
                let count = node.named_child_count();
                if count == 0
                    || (count == 1
                        && node.named_child(0).unwrap().kind() == "parenthesized_expression")
                {
                    pretty_children(node, _src)
                } else {
                    format!(
                        "return ({});\n",
                        pretty(&node.named_child(0).unwrap(), _src)
                    )
                }
            })
        }
        "comma_expression" => {
            format!("{};\n{}",
                pretty(&node.child_by_field_name("left").unwrap(), src),
                pretty(&node.child_by_field_name("right").unwrap(), src))
        },

        "," => String::from(", "),
        "}" => String::new(),
        ";" => String::from(";\n"),
        "return" => String::from("return "),
        "=" | "+" | "-" | "/" | "^" | "~" | "|" | "||" | "&&" | "==" => format!(" {} ", kind),
        "update_expression"|
        "expression_statement" => {
            match node.parent() {
                Some(parent) => if kind == "update_expression" && parent.kind() == "expression_statement" {
                    return pretty_children(node, src);
                },
                None => todo!(),
            }
            utils::indent(node, src, &pretty_children)
        },

        "call_expression"
        | "argument_list"
        | "assignment_expression"
        | "pointer_expression"
        | "binary_expression"
        | "pointer_declarator"
        | "function_definition"
        | "parameter_declaration"
        | "init_declarator"
        | "function_declarator"
        | "parameter_list"
        | "while_statement"
        | "if_statement"
        | "parenthesized_expression"
        | "parenthesized_declarator" => pretty_children(node, src),
        _ => String::from(token),
    }
}
