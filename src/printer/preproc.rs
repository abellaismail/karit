use crate::printer::utils;
use crate::source::Source;

use tree_sitter::Node;

pub fn print(node: &Node, src: &mut Source) {
    let exp = node
        .children(&mut node.walk())
        .filter(|item| !item.kind().trim().is_empty())
        .map(|item| String::from(utils::get_text(&item, src)))
        .collect::<Vec<String>>()
        .join(" ");

    src.preproc.push(exp)
}
