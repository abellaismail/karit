use crate::source::Source;
use tree_sitter::Node;

pub fn print(node: &Node, src: &mut Source) {
    let exp = node.utf8_text(src.code.as_bytes()).unwrap();
    let result;
    if exp.starts_with("//") {
        result = match exp.get(2..(exp.len())) {
            Some(comment) => String::from(format!("/* {} */", comment)),
            None => panic!("unexpected error in comments print"),
        }
    } else {
        result = String::from(exp);
    }

    if src.is_header {
        src.header.push(result);
    } else {
        src.body.push(result);
    }
}
