use crate::printer::universal;
use crate::source::Source;

use tree_sitter::Node;

// fun module
pub fn print(node: &Node, src: &mut Source) {
    print!("{}", universal::pretty(node, src));
}
