use crate::printer::utils;
use crate::source::Source;

use regex::Regex;
use tree_sitter::QueryCursor;
use tree_sitter::TextProvider;
use tree_sitter::{Language, Node, Query};

use super::universal;
use super::utils::indent;

extern "C" {
    fn tree_sitter_c() -> Language;
}

fn text_provider(input: &str) -> impl TextProvider<'_> {
    |node: Node<'_>| std::iter::once(input[node.byte_range()].as_bytes())
}

pub fn pretty_declarations(node: &Node, src: &mut Source) -> String {
    let language = unsafe { tree_sitter_c() };
    let q = Query::new(language, "((declaration) @dec)").unwrap();
    let mut cursor = QueryCursor::new();

    let mut res = cursor
        .captures(&q, *node, text_provider(src.code))
        .map(|(qm, _)| {
            let item = qm.captures[0].node;
            pretty(&item, src)
        })
        .collect::<Vec<String>>()
        .join("");
    if !res.is_empty() {
        res.push('\n');
    }
    res
}

pub fn pretty_take_identifier(node: &Node, src: &mut Source) -> String {
    match node.kind() {
        "init_declarator" => node
            .children(&mut node.walk())
            .map(|item| pretty_take_identifier(&item, src))
            .collect::<Vec<String>>()
            .join(""),
        "identifier" => String::from(utils::get_text(&node, src)),
        "pointer_declarator" => universal::pretty(node, src),
        _ => String::new(),
    }
}

pub fn pretty(node: &Node, src: &mut Source) -> String {
    let re = Regex::new(r"\s+").unwrap();
    let type_node = node.child_by_field_name("type").unwrap();
    let r#type = &src.code[type_node.start_byte()..type_node.end_byte()];
    let r#type = &re.replace_all(r#type, " ");
    node.children_by_field_name("declarator", &mut node.walk())
        .map(|item| {
            indent(&item, src, &|_, _src| {
                format!("{}\t{};\n", r#type, pretty_take_identifier(&item, _src))
            })
        })
        .collect::<Vec<String>>()
        .join("")
}

pub fn pretty_take_init(node: &Node, src: &mut Source) -> String {
    node.children_by_field_name("declarator", &mut node.walk())
        .filter(|item| item.kind() == "init_declarator")
        .map(|item| {
            indent(node, src, &|_, _src| {
                format!("{};\n", universal::pretty(&item, _src))
            })
        })
        .collect::<Vec<String>>()
        .join("")
}
