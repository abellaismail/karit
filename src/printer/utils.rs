use crate::source::Source;
use tree_sitter::Node;

pub fn indent(node: &Node, src: &mut Source, f: &dyn Fn(&Node, &mut Source) -> String) -> String {
    format!(
        "{}{}",
        (0..src.level).map(|_| "\t").collect::<String>(),
        f(node, src)
    )
}

pub fn get_text<'a>(node: &Node, src: &'a Source) -> &'a str {
    let source = src.code;
    &source[node.start_byte()..node.end_byte()]
}

pub fn get_text_by_field<'a>(node: &Node, src: &'a Source, name: &str) -> &'a str {
    let field = node.child_by_field_name(name);

    match field {
        Some(item) => get_text(&item, src),
        None => panic!("can't get field"),
    }
}
