use crate::printer::utils;
use crate::source::Source;

use tree_sitter::Node;

pub fn print(node: &Node, src: &mut Source) {
    let exp = node
        .children(&mut node.walk())
        .filter(|item| !item.kind().trim().is_empty())
        .map(|item| {
            let kind = item.kind();
            let token = utils::get_text(&item, src);
            match kind {
                "primitive_type" => format!("{}\t", token),
                _ => String::from(token),
            }
        })
        .collect::<Vec<String>>()
        .join("");

    src.gvars.push(exp)
}
