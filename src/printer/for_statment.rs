use crate::printer::universal;
use crate::source::Source;
use tree_sitter::Node;

pub fn for_convert(node: &Node, src: &mut Source) -> String {
    let initializer = node.child_by_field_name("initializer");
    let condition = node.child_by_field_name("condition");
    let update = node.child_by_field_name("update");

    let init = match initializer {
        Some(init_node) => universal::pretty(&init_node, src),
        None => String::new(),
    };

    let cond = match condition {
        Some(init_node) => universal::pretty(&init_node, src),
        None => String::from("1"),
    };

    src.level += 1;
    let update = match update{
        Some(init_node) => universal::pretty(&init_node, src),
        None => String::new(),
    };
    src.level -= 1;

    let body = match node.children(&mut node.walk()).find(|item| item.kind() == "compound_statement") {
        Some(init_node) => universal::pretty(&init_node, src),
        None => String::new(),
    };
    let indent = (0..src.level).map(|_| "\t").collect::<String>();

    format!("{}{}while ({})\n{}{{\n {} {};\n{}}}\n", init, indent, cond, indent, body, update, indent)
}
