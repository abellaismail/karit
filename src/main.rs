mod norm;
mod printer;
mod source;

use std::env;
use std::fs;
use tree_sitter::{Language, Parser, Tree};

extern "C" {
    fn tree_sitter_c() -> Language;
}

struct Config {
    filename: String,
}

fn parse_arg(args: &[String]) -> Config {
    let filename = args[1].clone();
    Config { filename }
}

fn get_abs_tree(source_code: &String) -> Option<Tree> {
    let mut parser = Parser::new();
    let language = unsafe { tree_sitter_c() };

    parser.set_language(language).unwrap();
    parser.parse(source_code, None)
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        panic!("only one argument is required");
    }
    let config = parse_arg(&args);
    let contents = fs::read_to_string(config.filename);

    let lines = match contents {
        Ok(lines) => lines,
        Err(err) => panic!("Problem reading the file: {:?}", err),
    };

    match get_abs_tree(&lines) {
        Some(tree) => norm::norm_tree(tree, &lines),
        None => panic!("Problem while parsing file"),
    }
}
