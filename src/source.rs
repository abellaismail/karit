#[derive(Debug)]
pub struct Source<'a> {
    pub code: &'a str,
    pub header: Vec<String>,
    pub preproc: Vec<String>,
    pub gvars: Vec<String>,
    pub body: Vec<String>,
    pub is_header: bool,
    pub level: u16,
}
