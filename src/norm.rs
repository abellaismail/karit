use crate::printer;
use crate::source::Source;

use tree_sitter::{Node, Tree};

fn print_src(src: &mut Source) {
    src.header
        .iter()
        .chain(src.preproc.iter())
        .chain(src.gvars.iter())
        .chain(src.body.iter())
        .for_each(|line| println!("{}", line));
}

pub fn norm_tree(tree: Tree, source: &str) {
    let mut src = Source {
        code: source,
        header: Vec::new(),
        preproc: Vec::new(),
        gvars: Vec::new(),
        body: Vec::new(),
        is_header: true,
        level: 0,
    };

    let root_node = tree.root_node();

    root_node
        .children(&mut root_node.walk())
        .for_each(|node: Node| {
            match node.kind() {
                "comment" => printer::comments::print(&node, &mut src),
                "preproc_include" => printer::preproc::print(&node, &mut src),
                "declaration" => printer::g_declaration::print(&node, &mut src),
                "function_definition" => printer::fun::print(&node, &mut src),
                _ => {
                    // src.body.push(String::from(node.kind()))
                }
            }
            if !node.kind().trim().is_empty() {
                src.is_header = node.kind() == "comment" && src.is_header
            }
        });
    // TODO: if not empty
    src.header.push(String::new());
    src.preproc.push(String::new());
    src.gvars.push(String::new());
    print_src(&mut src);
}
