# Karit (under development)
Karit is a command line tool that format your code
according to 42 `norm v3` standards,
so you can write it in the way you like and let `Karit` do the rest.

# We can We can't
We can handle most norm errors but we can't handle :

1. 25 line limit (but we will warn you).
1. there is no guarantee to work if you have a syntax error.

# Requirements
norminette to test if Karit did it <3

# checklist 
here is the current state:

| Norm           | implemented      | mostly works |
|----------------|------------------|--------------|
| 42header		 |         ✗        |      ✗       |
| preproc inc	 |         ✗        |      ✗       |
| global vars	 |         ✗        |      ✗       |
| func protoypes |         ✗        |      ✗       |

# Karit depends on
Karit is faster then norminette it self,
since it built with rust and `tree-sitter` library

I have no idea if tree-sitter is a good choice or not

# algorithm
Karit takes abstract syntax tree from `tree-sitter`
and then we use pretty printer algorithm
